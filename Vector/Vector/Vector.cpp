#include "Vector.h"

// constructor 
Vector::Vector(int n)
{
	const int MIN_SIZE = 2;

	if (n < MIN_SIZE) // n has to be at least 2
		n = MIN_SIZE;

	// initialize fields
	this->_size = 0;
	this->_resizeFactor = n; // resize factor is n
	this->_capacity = this->_resizeFactor;
	this->_elements = new int[this->_capacity +1];
}

// destructor
Vector::~Vector()
{
	delete[] this->_elements;
	this->_elements = nullptr;
}


// getters
int Vector::size() const { return this->_size; } //return size of vector
int Vector::capacity() const { return this->_capacity; } //return capacity of vector
int Vector::resizeFactor() const { return this->_resizeFactor; } //return vector's resizeFactor
bool Vector::empty() const { return this->_size == 0; } //returns true if size = 0


// Adds a new element at the end of the vector, after its current last element. The content of val is copied to the new element. (disc was taken from the cpp site XD)
void Vector::push_back(const int& val)
{
	if (this->_capacity <= this->_size) // if out of space
		reserve(this->_capacity + this->_resizeFactor); // resizes the array

	if (this->_capacity > this->_size) // prevent buffer overflow
	{
		this->_elements[this->_size] = val; // add new value
		this->_size++; // update size
	}
}

// Removes the last element in the vector, effectively reducing the container size by one. (disc was taken from the cpp site as well XD)
int Vector::pop_back()
{
	const int EMPTY_VECTOR = -9999;
	int lastElement;

	if (empty()) // if there are no elements in the array
	{
		std::cout << "error: pop from empty vector" << std::endl;
		lastElement = EMPTY_VECTOR;
	}
	else
	{
		this->_size -= 1; // one less element (also makes the size value the index of the last element)
		lastElement = this->_elements[this->_size];
	}

	return lastElement;
}

// makes sure that the capacity is at least n
void Vector::reserve(int n)
{
	if (this->_capacity < n) // make sure that there isnt enough space reserved already
	{
		int* temp = new int[this->_size]; // a temp array to hold the elements of the array

		for (int i = 0; i < this->_size; i++)
			temp[i] = this->_elements[i];

		while (this->_capacity < n) // increase capacity until reserved the wanted amount
			this->_capacity += this->_resizeFactor;

		this->_elements = new int[this->_capacity];

		if (this->_size <= this->_capacity) // prevent buffer overflow
			for (int i = 0; i < this->_size; i++) {
				this->_elements[i] = temp[i];
			}

		delete[] temp;
		temp = nullptr;
	}
}

// Resizes the container so that it contains n elements.
void Vector::resize(int n)
{
	const int DEFAULT_VAL = 0;
	resize(n, DEFAULT_VAL);
}

// Resizes the container so that it contains n elements. each new element is equal to val
void Vector::resize(int n, const int& val)
{
	if (this->_capacity < n) // if n is grater then the current capacity, reserve more storage space
		reserve(n);

	// the content is expanded  by inserting at the end as many elements as needed to reach a size of n.
	for (int i = this->_size; i < n; i++)
		this->_elements[i] = val;

	this->_size = n; // update size
}

// assigns val to all elemnts
void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++) // go through the vector from the first element to the last 
		this->_elements[i] = val; // each element in the vector is val
}


// deep copy constructor (we need to implement this because we have a pointer in our object)
Vector::Vector(const Vector& other)
{
	*this = other; // uses copy operator - copy other object to this object
}

// operators

// copy operator
Vector& Vector::operator=(const Vector& other)
{
	if (this == &other) // tries to copy the object itself
		return *this;

	// shallow copy fields
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;

	// deep copy dynamic field (elements)
	this->_elements = new int[this->_capacity];
	if (this->_size <= this->_capacity) // prevent buffer overflow
		for (int i = 0; i < this->_size; i++)
		{
			this->_elements[i] = other._elements[i]; //copies cell by cell
		}

	return *this;
}

//Element Access n'th element
int& Vector::operator[](int n) const
{
	if (n < this->_size && 0 <= n) // check if index is valid
	{
		return this->_elements[n];
	}
	else // if index is not valid
	{
		std::cerr << "array index " << n << " is past the end of the array (which contains " << this->_size << " elements)" << std::endl;
		return this->_elements[0]; // returns first element
	}
}

// v3 will have the same attributes as v1
Vector& Vector::operator+(const Vector& other) const
{
	static Vector v3 = *this;
	v3 += other;

	return v3;
}

// v3 will have the same attributes as v1 
Vector& Vector::operator-(const Vector& other) const
{
	static Vector v3 = *this;
	v3 -= other;

	return v3;
}

void Vector::operator-=(const Vector& other)
{
	Vector v2 = other;

	// make sure that both of the vectors have the same amount of elements
	if (this->_size < v2._size) // if v2 has more elements
		this->resize(v2._size);

	else if (v2._size < this->_size)
		v2.resize(this->_size);

	for (int i = 0; i < v2._size; i++)
		this->_elements[i] -= v2._elements[i]; // adds the numbers in each vector
}

void Vector::operator+=(const Vector& other)
{
	Vector v2 = other;

	// make sure that both of the vectors have the same amount of elements
	if (this->_size < v2._size) // if v2 has more elements
		this->resize(v2._size);

	else if (v2._size < this->_size)
		v2.resize(this->_size);

	for (int i = 0; i < v2._size; i++)
		this->_elements[i] += v2._elements[i]; // subs the numbers in each vector
}

std::ostream& operator << (std::ostream& out, const Vector& v)
{
	out << "Vector Info: \nCapacity is " << v._capacity; 
	out << "\nSize is " << v._size;
	out << "\ndata is {";
	for (int i = 0; i < v._size; i++)
	{
		out << v._elements[i];
		if (i < v._size - 1)
			out << ",";
	}
	out << "}\n";

	return out;
}